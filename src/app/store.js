import { configureStore } from '@reduxjs/toolkit';
import playState from '../features/playState';
import musicLibaryList from '../features/musicLibaryList';
import trackState from '../features/trackState';
import deviceState from '../features/deviceState';

export default configureStore({
  reducer: {
      play: playState,
      libaryList: musicLibaryList,
      track: trackState,
      device: deviceState,
  },
})