import React from "react";
import './Button.css'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import getUser from '../api/getUser';
import ScheduleIcon from '@material-ui/icons/Schedule';

import { setIsPlaying } from '../features/playState';
import { setValue, setCurrentMS } from '../features/trackState';
import { setTrack } from '../features/trackState.js';

import { connect } from "react-redux";
import Grid from '@material-ui/core/Grid';

class BrowserList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tracks: [],
            trackSelected: null,
        }
    }

    async handleURI(uri, id, mili, playlist, track) {
        getUser.playSelectedTrack(this.props.device, uri, playlist);
        this.props.setIsPlaying();
        this.setState({"trackSelected": id});
        this.props.setValue(mili);
        this.props.setCurrentMS(0);
        this.props.setTrack(track);
        
    }

     millisToMinutesAndSeconds(millis) {
        var minutes = Math.floor(millis / 60000);
        var seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
      }

    render() {

        return (
            <div className="browserlist">
                <div className="gradient"></div>
                {this.props.musicLibaryList.value.tracks.items.length > 1 &&
                    <div className="playlist-header">
                        <Grid container xs={12}>
                            <Grid  item xs={4}>
                                <Grid  item xs={12}>
                                    <img className="image-browser" src={this.props.musicLibaryList.value.images[1].url} />
                                </Grid>
                            </Grid>
                            <Grid className={"header-text"} item xs={8}>
                                <h2> { this.props.musicLibaryList.value.type }</h2>
                                <h1> { this.props.musicLibaryList.value.name }</h1>

                                <span> { this.props.musicLibaryList.value.owner.display_name}</span>
                                <span> * </span>
                                <span> { this.props.musicLibaryList.value.followers.total}</span>
                                <span> Likes </span>
                                <span> * </span>
                                <span> { this.props.musicLibaryList.value.tracks.total}</span>
                                <span> Songs </span>
                            </Grid>
                        </Grid>
                    </div>
                }
                <TableContainer>
                    <Table className={"table-list"} size="small" aria-label="a dense table" stickyHeader>
                        <TableHead>
                            <TableRow>
                                <TableCell className="table-head-cell-mlist" align="left">
                                    <span>#</span>
                                </TableCell>
                                <TableCell className="table-head-cell-mlist" align="left">
                                    <span>Title</span>
                                </TableCell>
                                <TableCell className="table-head-cell-mlist" align="left">
                                    <span>Album</span>
                                </TableCell>
                                <TableCell className="table-head-cell-mlist" align="left">
                                    <span>Date</span>
                                </TableCell>
                                <TableCell className="table-head-cell-mlist" align="left">
                                    <span>
                                        <ScheduleIcon/>
                                    </span>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {this.props.musicLibaryList.value.tracks.items.length > 1 &&
                            this.props.musicLibaryList.value.tracks.items.map((item, index) => (
                                <TableRow className="table-cell-row-mlist" key={item.track.id} selected={ index === this.state.trackSelected ? true : false } hover onClick={() => this.handleURI(index, index, item.track.duration_ms, this.props.musicLibaryList.value.uri, item.track)}>
                                    <TableCell className="table-cell-mlist" component="th" scope="row">
                                        <span className="id">{ index }</span>
                                    </TableCell>
                                    <TableCell className="table-cell-mlist" component="th" scope="row">
                                        <Grid container>
                                            <Grid item xs={2}>
                                                <img className={"image-table"} src={item.track.album.images[2].url} />
                                            </Grid>
                                            <Grid item xs={10}>
                                                <p className={"title"}>{item.track.name}</p>
                                                <p className={"author table-active-mlist"}>{item.track.artists[0].name}</p>
                                            </Grid>
                                        </Grid>
                                    </TableCell>
                                    <TableCell className="table-cell-mlist table-active-mlist"><span>{item.track.album.name}</span></TableCell>
                                    <TableCell className="table-cell-mlist"><span>{item.added_at}</span></TableCell>
                                    <TableCell className="table-cell-mlist"><span>{this.millisToMinutesAndSeconds(item.track.duration_ms)}</span></TableCell>
                                </TableRow>
                            ))
                        }
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        );
    }
}

const mapStateToProps = state => ({ 
    playState: state.play.value,
    musicLibaryList: state.libaryList,
    device: state.device.value,
});

const mapDispatchToProps = (dispatch) => { 
    return {
        setIsPlaying: () => dispatch(setIsPlaying()),
        setValue: (payload) => dispatch(setValue(payload)),
        setCurrentMS: (payload) => dispatch(setCurrentMS(payload)),
        setTrack: (payload) => dispatch(setTrack(payload)),
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(BrowserList)