import React from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { BsFillPlayFill } from "react-icons/bs";
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { MdComputer } from 'react-icons/md';
import { connect } from "react-redux";

import DevicesIcon from '@material-ui/icons/Devices';

import getUser from "../api/getUser";

import { setValue } from '../features/deviceState';

class Devices extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            response: [{
                name: "",
                id: ""
            }],
            anchorEl: null,
            selectedIndex: 0
        };
    }

    async componentDidMount() {
        this.setState({ response: await getUser.getDevices() });

        this.props.setValue( this.state.response[0].id );
        
    }

    handleClickListItem = (event) => {
        this.setState({ anchorEl: event.currentTarget });
      };
    
    handleMenuItemClick = (event, index, id) => {
        this.setState({ selectedIndex: index });
        this.setState({ anchorEl: null });
        this.props.setValue( id );
    };
    
    handleClose = () => {
        this.setState({ anchorEl: null });
    };


    render() {
        return (
            <div>
                <List component="nav" aria-label="Device settings">
                <ListItem
                button
                aria-haspopup="true"
                aria-controls="lock-menu"
                aria-label="Selected Device"
                onClick={this.handleClickListItem}
                >
                <DevicesIcon  color="secondary" />
                </ListItem>
            </List>
            <Menu

                anchorEl={this.state.anchorEl}
                keepMounted
                open={Boolean(this.state.anchorEl)}
                onClose={this.handleClose}
            >
                {this.state.response.map((item, index) => (
                <MenuItem
                    key={item.id}
                    selected={index === this.state.selectedIndex}
                    onClick={(event) => this.handleMenuItemClick(event, index, item.id)}
                >
                    <MdComputer />
                    {item.name}
                </MenuItem>
                ))}
            </Menu>
            </div>
        );
    }
}

const mapStateToProps = state => ({ 
    device: state.play.value,
});

const mapDispatchToProps = (dispatch) => { 
    return {
        setValue: (payload) => dispatch(setValue(payload)),
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(Devices)