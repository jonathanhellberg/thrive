import React from "react";
import { BsFillPlayFill } from "react-icons/bs";
import { Fab } from '@material-ui/core';
import './Button.css'
import getUser from "../api/getUser";
import { connect } from "react-redux";

import { setValue } from '../features/deviceState';

class PlayButton extends React.Component {

    constructor(props) {
        super(props);
    }

    playButton() {
        getUser.playDevice(this.props.device);
    }

    render() {
        return (
            <div>
                <Fab className="play-button" aria-label="Play" onClick={() => this.playButton()}>
                    <BsFillPlayFill className='PlayIcon' />
                </Fab>
            </div>
        );
    }
}

const mapStateToProps = state => ({ 
    device: state.device.value,
});

const mapDispatchToProps = (dispatch) => { 
    return {
        setValue: (payload) => dispatch(setValue(payload)),
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(PlayButton)