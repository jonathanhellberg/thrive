import React from "react";
import { BsFillSkipStartFill } from "react-icons/bs";
import './Button.css'
import getUser from "../api/getUser";

export default class PreSongButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            deviceId: ""
        }
    }

    skipPrevSong(id) {
        console.log(id);
        getUser.prevSong(id);
    }

    render() {
        return (
            <div className="margin-buttons">
                <BsFillSkipStartFill className='PreSongIcon' onClick={() => this.skipPrevSong(this.props.deviceId)} />
            </div>
        );
    }
}