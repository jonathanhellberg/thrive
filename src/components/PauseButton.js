import React from "react";
import { BsFillPauseFill } from "react-icons/bs";
import { Fab } from '@material-ui/core';
import './Button.css'
import getUser from "../api/getUser";
import { connect } from "react-redux";

import { setValue } from '../features/deviceState';

class PauseButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            deviceId: ""
        }
    }

    pauseButton() {
        getUser.pauseSong(this.props.device);
    }

    render() {
        return (
            <div>
                <Fab className="pause-button" aria-label="Pause" onClick={() => this.pauseButton()}>
                    <BsFillPauseFill className='PlayIcon' />
                </Fab>
            </div>
        );
    }
}

const mapStateToProps = state => ({ 
    device: state.device.value,
});

const mapDispatchToProps = (dispatch) => { 
    return {
        setValue: (payload) => dispatch(setValue(payload)),
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(PauseButton)
