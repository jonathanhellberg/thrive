import React from "react";
import MusicBar from './MusicBar';
import MusicLibary from './MusicLibary';

import Grid from '@material-ui/core/Grid';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';

import BrowserList from './BrowserList';

import './Button.css'

import {
    Link
  } from "react-router-dom";
import { Button } from "@material-ui/core";

export default class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            deviceId: ""
        }
        this.getDeviceID = this.getDeviceID.bind(this);
    }

    getDeviceID(id) {
        this.setState({ deviceId: id })
    }

    render() {
        return (
            <div>
                <Grid item xs={12}>
                    <Grid container justify="center">
                        <div>
                            <Drawer
                                variant="permanent"
                                anchor="left"
                                className="Drawer-color"
                            >
                            <Link className="link-route" to="/">
                                <Button>
                                    <HomeOutlinedIcon className="icon-route" fontSize="large" />
                                    <span className="text-route">Home</span>
                                </Button>
                            </Link>

                            <Divider className="drawer-divider" variant="middle" />
                                <MusicLibary />
                            </Drawer>
                        </div>
                        <div className="test">
                            <BrowserList deviceId={this.state.deviceId} />
                        </div>
                    </Grid>
                </Grid>


                <MusicBar getDeviceID={this.getDeviceID} />

            </div>
        );
    }
}
