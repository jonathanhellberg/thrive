import React from "react";
import Slider from '@material-ui/core/Slider';
import './Button.css'

import Grid from '@material-ui/core/Grid';

import { useSelector } from "react-redux";
import { useEffect } from 'react';

import { setCurrentMS } from '../features/trackState'

import { connect } from "react-redux";

function millisToMinutesAndSeconds(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
  }

function MusicPlayerSlider(props){

    const currentMS = useSelector(state => state.track.currentMS);


    const value = useSelector(state => state.play.value);
    let trackMS = useSelector(state => state.track.value);

    const displayCurrentMS = millisToMinutesAndSeconds(currentMS * 1000)

    const MaxMS = millisToMinutesAndSeconds(trackMS);

    trackMS = (trackMS / 1000).toFixed(0);

    useEffect(() => {
            const interval = setInterval(() => {
                if(value && currentMS <= trackMS - 1) {
                    props.setCurrentMS(currentMS + 1);
                }
            }, 1000
            );

        return () => clearInterval(interval); // This represents the unmount function, in which you need to clear your interval to prevent memory leaks.
      }, [currentMS, value])

        return (
            <div>
                
                <Grid container spacing={0} className="slider">
                    <Grid item xs={2}>
                        <span className="ColorPrimary">{displayCurrentMS}</span>
                    </Grid>
                    <Grid item xs={8}>
                        <Slider
                            value={currentMS}
                            max={trackMS}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <span className="ColorPrimary" >{MaxMS}</span>
                    </Grid>
                </Grid>
            </div>
        );
}

const mapStateToProps = state => ({ 
    trackState: state.track
});

const mapDispatchToProps = (dispatch) => { 
    return {
        setCurrentMS: (payload) => dispatch(setCurrentMS(payload))
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(MusicPlayerSlider)