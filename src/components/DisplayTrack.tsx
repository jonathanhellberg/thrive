import React from "react";
import track from "../api/track";

import { Data } from "../@types/playback";

import './main.css'

import { connect } from "react-redux";

import { setValue } from '../features/deviceState';
import { setCurrentMS, 
        setValue as setSongLength,
        setTrack,
       } from '../features/trackState.js';

import device from "../api/device";

type MyState = {
  response: any,
  recived: boolean
};


class DisplayTrack extends React.Component<any, MyState> {

  state: MyState = {
    response: null,
    recived: false,
  };

  async componentDidMount() {

    await device.setDevice(this.props.device);
    
    const data = await track.getTracks();

    this.props.setTrack(data.item);
    this.props.setCurrentMS(data.progress_ms / 1000);
    this.props.setSongLength(data.item.duration_ms);

    console.log("RESPONE!!!!!!!!")
    console.log(this.state.response)
    console.log("-----------------------")

    this.setState({response: this.props.track});

    this.setState({recived: true});
  }

    render() {
      return (
        <div>
          {this.state.recived === true && 
          <div className="device-box">
            <div className="device-img">
              <img src={ this.props.track.album.images[2].url} />
            </div>
            <div className="device-text">
              <span className={"song-name"}>{this.props.track.name}</span>
              <span className={"artists"}>{this.props.track.artists[0].name}</span>
            </div>
          </div>
          }

        </div>
      );
    }
}

const mapStateToProps = state => ({ 
  device: state.device.value,
  track: state.track.track,
});

const mapDispatchToProps = (dispatch) => { 
  return {
      setValue: (payload) => dispatch(setValue(payload)),
      setCurrentMS: (payload) => dispatch(setCurrentMS(payload)),
      setSongLength: (payload) => dispatch(setSongLength(payload)),
      setTrack: (payload) => dispatch(setTrack(payload)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DisplayTrack)