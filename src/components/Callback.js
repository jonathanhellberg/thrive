import React from "react";
import { useLocation } from "react-router-dom";
import Auth from "../api/auth"

export default function Callback() {
    const query = new URLSearchParams(useLocation().search);

    checkCode(query);

    return ( 
        <div>
        </div>
        );
}

function checkCode(query) {
    
    console.log(query.get("code"));

    if (query.get("code") != null) {
        Auth.getToken(query.get("code"));
    }

}