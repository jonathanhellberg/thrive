import React from "react";
import Auth from "../api/auth";
import { CLIENT_ID } from "../api/config"

export default function Login(){
    
    function onPress() {
        let data = Auth.getAuth();

        console.log(data)
    }

    return (
        <div>
            <button onClick={onPress}> Log in with Spotify</button>
        </div>
    );
}