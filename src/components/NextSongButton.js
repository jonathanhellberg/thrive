import React from "react";
import { BsFillSkipEndFill } from "react-icons/bs";
import './Button.css'
import getUser from "../api/getUser";

export default class NextSongButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            deviceId: ""
        }
    }

    skipNextSong(id) {
        console.log(id);
        getUser.nextSong(id);
    }

    render() {
        return (
            <div className="margin-buttons-next">
                <BsFillSkipEndFill className='PreSongIcon' onClick={() => this.skipNextSong(this.props.deviceId)}/>
            </div>
        );
    }
}

