import React from "react";
import PlayButton from "./PlayButton";
import PreSongButton from "./PreSongButton";
import NextSongButton from "./NextSongButton";
import Grid from '@material-ui/core/Grid';
import './Button.css'
import MusicPlayerSlider from "./MusicPlayerSlider";
import getUser from "../api/getUser";
import Devices from "./Devices";
import PauseButton from "./PauseButton";
import { setNotPlaying, setIsPlaying } from '../features/playState'

import { connect } from "react-redux";

import DisplayTrack from "./DisplayTrack.tsx";


class MusicBar extends React.Component {

    constructor() {
        super();
        this.handlePlayClick = this.handlePlayClick.bind(this);
        this.handlePauseClick = this.handlePauseClick.bind(this);
    }
    
    componentDidMount() {
        getUser.getUser();
    }

    handlePlayClick() {
        this.props.setIsPlaying()
    }
    
    handlePauseClick() {
        this.props.setNotPlaying()
    }

        render () {
            
            const currentlyPlaying = this.props.playState;
            let playButton;
            
            if(this.props.device !== null){
                if (!currentlyPlaying) {
                    playButton = <div onClick={this.handlePlayClick}><PlayButton deviceId={this.props.device.value} /></div>;
                } else {
                    playButton = <div onClick={this.handlePauseClick}><PauseButton  deviceId={this.props.device.value} /></div>;
                }
            }
            return (
                <div>
                    
                    <Grid className="MusicBar" container spacing={0}>
                        <Grid item xs={3}>
                            <DisplayTrack />
                        </Grid>
                        <Grid item xs={6}>
                            <Grid container spacing={12} className="center" justify = "center">
                                    <Grid item xs={12}>
                                        <Grid container spacing={3} className="center" justify = "center">
                                            <PreSongButton />
                                            { playButton }
                                            <NextSongButton />
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <MusicPlayerSlider />
                                    </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={3}>
                        <Devices />
                        </Grid>
                    </Grid>
                </div>
            );
        }
}

const mapStateToProps = state => ({ 
    playState: state.play.value,
    device: state.device.value,
});

const mapDispatchToProps = (dispatch) => { 
    return {
        setNotPlaying: () => dispatch(setNotPlaying()),
        setIsPlaying: () => dispatch(setIsPlaying())
    }
 }

 export default connect(mapStateToProps, mapDispatchToProps)(MusicBar)