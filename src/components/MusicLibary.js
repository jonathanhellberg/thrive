import React from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LibaryAPI from '../api/LibaryAPI';
import './musiclist.css'

import { setNewBrowserList } from '../features/musicLibaryList'

import { connect } from "react-redux";

class MusicLibary extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            response: [],
            tracks: []
        }
    }

    async componentDidMount() {
        this.setState({ response: await LibaryAPI.getLibary() });
    }

    async handleClick(id) {
        this.setState({ tracks: await LibaryAPI.getTracks(id) });
        this.props.setNewBrowserList(this.state.tracks);
        console.log(this.state.tracks)
    }

    render() {

        return (
            <div>
                <List className="ListRoot" component="nav" aria-label="secondary mailbox folders">
                {this.state.response.map((item, index) => (
                    <ListItem classes={{root: "listitem"}} button component="a" onClick={() => this.handleClick(item.id)}>
                        <ListItemText classes={{primary: "listitemtext"}} primary={item.name} />
                    </ListItem>
                ))}
                </List>
            </div>
        );
    }
}

const mapStateToProps = state => ({ 
    musicLibaryList: state.libaryList
});

const mapDispatchToProps = (dispatch) => { 
    return {
        setNewBrowserList: (payload) => dispatch(setNewBrowserList(payload))
    }
 }

export default connect(mapStateToProps, mapDispatchToProps)(MusicLibary)