export const CLIENT_ID = "";

export const CLIENT_SECRET = "";

export const CALLBACK_URL = "http://localhost:3000/callback";

export const SCOPES = 'user-read-private user-read-email user-read-playback-state user-modify-playback-state playlist-read-private playlist-read-collaborative user-read-currently-playing';