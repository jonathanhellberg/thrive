import axios from 'axios';

export default {

    getLibary: async function() {
        const token_type = localStorage.getItem('token_type');
        const access_token = localStorage.getItem('access_token');

        const headers = {
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Authorization': token_type + " " + access_token
        }
        
        return axios({
            method: 'GET',
            url: 'https://api.spotify.com/v1/me/playlists',
            headers
          }).then((response) => {
            console.log(response);
            return response.data.items;
          }, (error) => {
            console.log(error);
          });
    },

    getTracks: async function(playlist_id) {
      const token_type = localStorage.getItem('token_type');
      const access_token = localStorage.getItem('access_token');

      const headers = {
          'Accept': 'application/json',
          'Content-Type':'application/json',
          'Authorization': token_type + " " + access_token
      }
      
      return axios({
          method: 'GET',
          url: 'https://api.spotify.com/v1/playlists/' + playlist_id,
          headers
        }).then((response) => {
          console.log(response);
          return response.data;
        }, (error) => {
          console.log(error);
        });
  },

}