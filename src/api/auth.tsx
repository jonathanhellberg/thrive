import axios from 'axios';
import qs from 'qs';
import { CLIENT_ID, CLIENT_SECRET, CALLBACK_URL, SCOPES } from './config'

export default {


    getAuth: async function() {
        try {
            const response = await window.location.replace('https://accounts.spotify.com/authorize' +
            '?response_type=code' +
            '&client_id=' + CLIENT_ID +
            (SCOPES ? '&scope=' + encodeURIComponent(SCOPES) : '') +
            '&redirect_uri=' + encodeURIComponent(CALLBACK_URL));
        }
        catch(error)
        {
            throw error;
        }
    },

    getToken: async function(code) {
        const encodedString = Buffer.from(CLIENT_ID + ":" + CLIENT_SECRET).toString('base64');
        const auth = 'Basic ' + encodedString;

        const headers = {
            'Content-Type':'application/x-www-form-urlencoded',
            'Authorization': auth
        }

        axios({
            method: 'POST',
            url: 'https://accounts.spotify.com/api/token',
            data: qs.stringify({
              grant_type: "authorization_code",
              code: code,
              redirect_uri: CALLBACK_URL,
            }),
            headers
          }).then((response) => {
            console.log(response);
            localStorage.setItem('access_token', response.data.access_token);
            localStorage.setItem('refresh_token', response.data.refresh_token);
            localStorage.setItem('token_type', response.data.token_type);
            window.location.replace('http://localhost:3000/');
          }, (error) => {
            console.log(error);
          });
    }
}