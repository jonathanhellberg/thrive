import axios, { AxiosResponse } from 'axios';

import { Data } from "../@types/playback";

export default {

    getTracks: async (): Promise<Data> => {
      const token_type = localStorage.getItem('token_type');
      const access_token = localStorage.getItem('access_token');

      const headers = {
          'Accept': 'application/json',
          'Content-Type':'application/json',
          'Authorization': token_type + " " + access_token
      }
      
      const response: AxiosResponse<Data> = await axios({
          method: 'GET',
          url: 'https://api.spotify.com/v1/me/player/currently-playing',
          headers
        })
      const payload = response.data;
      return payload;
  },

}