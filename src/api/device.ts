import axios from 'axios';

export default {

    setDevice: async function(device_id) {
      const token_type = localStorage.getItem('token_type');
      const access_token = localStorage.getItem('access_token');

      const headers = {
          'Accept': 'application/json',
          'Content-Type':'application/json',
          'Authorization': token_type + " " + access_token
      }

      const data = {
        device_ids: [
            device_id
        ]
      }
      
      return axios({
          method: 'PUT',
          url: 'https://api.spotify.com/v1/me/player',
          headers,
          data
        }).then((response) => {
          console.log("Device is Connected");
        }, (error) => {
          console.log(error);
        });
  },

}