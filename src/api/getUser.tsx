import axios from 'axios';

export default {
    getUser: async function() {
        const token_type = localStorage.getItem('token_type');
        const access_token = localStorage.getItem('access_token');

        const headers = {
            'Content-Type':'application/x-www-form-urlencoded',
            'Authorization': token_type + " " + access_token
        }

        axios({
            method: 'GET',
            url: 'https://api.spotify.com/v1/me',
            headers
          }).then((response) => {
            console.log("USER:");
            console.log(response);
            console.log("-----------------------------");
          }, (error) => {
            console.log(error);
          });
    },

    getDevices: async function() {
        const token_type = localStorage.getItem('token_type');
        const access_token = localStorage.getItem('access_token');

        const headers = {
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Authorization': token_type + " " + access_token
        }
        
        return axios({
            method: 'GET',
            url: 'https://api.spotify.com/v1/me/player/devices',
            headers
          }).then((response) => {
            console.log("Get Device: ");
            console.log(response);
            console.log("-----------------------------");
            return response.data.devices;
          }, (error) => {
            console.log(error);
          });
    },

    playDevice: async function(device_id) {
        const token_type = localStorage.getItem('token_type');
        const access_token = localStorage.getItem('access_token');

        const headers = {
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Authorization': token_type + " " + access_token
        }
        
        axios({
            method: 'PUT',
            url: 'https://api.spotify.com/v1/me/player/play?device_id=' + device_id,
            headers
          }).then((response) => {
            console.log(response);
            // return response.data.devices;
          }, (error) => {
            console.log(error);
          });
    },

    nextSong: async function(device_id) {
        const token_type = localStorage.getItem('token_type');
        const access_token = localStorage.getItem('access_token');

        const headers = {
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Authorization': token_type + " " + access_token
        }
        
        axios({
            method: 'POST',
            url: 'https://api.spotify.com/v1/me/player/next?device_id=' + device_id,
            headers
          }).then((response) => {
            console.log(response);
            // return response.data.devices;
          }, (error) => {
            console.log(error);
          });
    },

    prevSong: async function(device_id) {
        const token_type = localStorage.getItem('token_type');
        const access_token = localStorage.getItem('access_token');

        const headers = {
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Authorization': token_type + " " + access_token
        }
        
        axios({
            method: 'POST',
            url: 'https://api.spotify.com/v1/me/player/previous?device_id=' + device_id,
            headers
          }).then((response) => {
            console.log(response);
            // return response.data.devices;
          }, (error) => {
            console.log(error);
          });
    },

    pauseSong: async function(device_id) {
        const token_type = localStorage.getItem('token_type');
        const access_token = localStorage.getItem('access_token');

        const headers = {
            'Accept': 'application/json',
            'Content-Type':'application/json',
            'Authorization': token_type + " " + access_token
        }
        
        axios({
            method: 'PUT',
            url: 'https://api.spotify.com/v1/me/player/pause?device_id=' + device_id,
            headers
          }).then((response) => {
            console.log(response);
            // return response.data.devices;
          }, (error) => {
            console.log(error);
          });
    },

    playSelectedTrack: async function(device_id, track, playlist) {
      const token_type = localStorage.getItem('token_type');
      const access_token = localStorage.getItem('access_token');

      const headers = {
          'Accept': 'application/json',
          'Content-Type':'application/json',
          'Authorization': token_type + " " + access_token
      }

      console.log(playlist)

      const data = {
        context_uri: playlist,
        offset: {
          "position": track
        },
      
      }
      
      axios({
          method: 'PUT',
          url: 'https://api.spotify.com/v1/me/player/play?device_id=' + device_id,
          data,
          headers
        }).then((response) => {
          console.log(response);
          // return response.data.devices;
        }, (error) => {
          console.log(error);
        });
  },
}