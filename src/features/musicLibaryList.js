import { createSlice } from '@reduxjs/toolkit';

export const musicLibaryList = createSlice({
    name: 'musicLibaryList',
    initialState: {
        value: { 
            followers: {
                total: 0
            },
            tracks: { 
                items: [
                    { 
                        track: {
                            id: "", 
                            name: "", 
                            duration_ms: "", 
                            uri: "", 
                            artists: [
                                {
                                    name: "",
                                }
                            ], 
                            album: 
                            {
                                images: [
                                    {
                                        url: "",
                                    }
                                ]
                            } 
                        }
                    }
                ]
            },
        },
    },

    reducers: {
        setNewBrowserList(state, action) {
            state.value = action.payload;
        },
    }

})

export const { setNewBrowserList } = musicLibaryList.actions;

export default musicLibaryList.reducer;