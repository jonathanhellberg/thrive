import { createSlice } from '@reduxjs/toolkit';

export const playState = createSlice({
    name: 'playState',
    initialState: {
        value: false,
    },
    reducers: {
        setNotPlaying(state) {
            state.value = false
        },
        setIsPlaying(state) {
            state.value = true
        },
    }
})

export const { setNotPlaying, setIsPlaying } = playState.actions;

export default playState.reducer;