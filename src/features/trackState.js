import { createSlice } from '@reduxjs/toolkit';

export const trackState = createSlice({
    name: 'trackState',
    initialState: {
        value: 0,
        currentMS: 0,
        track: {
            id: "", 
            name: "", 
            duration_ms: "", 
            uri: "", 
            artists: [
                {
                    name: "",
                }
            ], 
            album: 
            {
            images: [
                    {
                        url: "",
                    }
                ]
            }
        }
    },
    reducers: {
        setValue(state, action) {
            state.value = action.payload;
        },
        setCurrentMS(state, action) {
            state.currentMS = action.payload;
        },
        setTrack(state, action) {
            state.track = action.payload;
        },
    }
})

export const { setValue, setCurrentMS, setTrack } = trackState.actions;

export default trackState.reducer;