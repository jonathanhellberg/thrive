import { createSlice } from '@reduxjs/toolkit';

export const deviceState = createSlice({
    name: 'deviceState',
    initialState: {
        value: null,
    },
    reducers: {
        setValue(state, action) {
            state.value = action.payload;
        }
    }
})

export const { setValue } = deviceState.actions;

export default deviceState.reducer;