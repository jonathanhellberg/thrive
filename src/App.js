import './App.css';
import Callback from './components/Callback'
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import Login from './components/Login';
import MainPage from './components/MainPage'

function App() {
  return (
    <Router>
      <div className="App">


          <Switch>
            <Route path="/" exact>
              <MainPage />
            </Route>
            <Route path="/callback">
                <Callback />
            </Route>
            <Route path="/login">
                <Login />
            </Route>
          </Switch>
      </div>
    </Router>
  );
}

export default App;
